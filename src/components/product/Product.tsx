import React, { useState } from 'react'
import { useFormik } from 'formik'
import { Box, Button, Input, Text, Table, Tbody, Td, Tr, Thead, Th, Center, Wrap, WrapItem, Heading } from '@chakra-ui/react'

type FieldData = {
  name: string,
  address: string,
  phone?: string,
}

type Data = FieldData[]

const defaultData: Data = [{
  name: "",
  address: "",
  phone: "",
}]

const Product = () => {
  const [data, setData] = useState(defaultData)


  const formik = useFormik({
    initialValues: {
      nama: '',
      address: '',
      phone: '',

    },
    onSubmit: (values) => {
      const newData = [...data]
      newData.push({ name: values.nama, address: values.address, phone: values.phone })
      setData(newData)
    },
  })
  return (
    <Box boxShadow='xl' p='6' rounded='md' bg='white'>
      <form onSubmit={formik.handleSubmit }
      >
        <Center>
        <Heading as='h3' size='lg' mb={5}>
            Data Produk
        </Heading>
        </Center>


        <Input
          mb={5}
          id='nama'
          name='nama'
          type='text'
          placeholder='Nama'
          onChange={formik.handleChange}
          value={formik.values.nama} />


        <Input
          mb={5}
          id='address'
          name='address'
          type='text'
          placeholder='Jenis'
          onChange={formik.handleChange}
          value={formik.values.address} />

        <Input
          mb={5}
          id='phone'
          name='phone'
          type='number'
          placeholder='Harga'
          onChange={formik.handleChange}
          value={formik.values.phone} />

        <Center>
        
        <Button type='submit' colorScheme='teal' >
          Add
        </Button>

        <Button ml={5} colorScheme='teal' variant='outline' onClick={() => {

          setData([])
          }}>
          Delete
        </Button>
        </Center>

        <Table colorScheme='teal' variant='striped'>
          <Thead>
            <Tr>
              <Th>Name</Th>
              <Th>Address</Th>
              <Th>Phone</Th>
            </Tr>
          </Thead>
          <Tbody>
            {
              data.map((d) => {
                return (
                  <Tr>
                    <Td>{d.name}</Td>
                    <Td>{d.address}</Td>
                    <Td>{d.phone}</Td>
                  </Tr>
                )
              })
            }
          </Tbody>
        </Table>

      </form>
    </Box>
  )
}

export default Product