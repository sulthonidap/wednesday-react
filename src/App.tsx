import * as React from "react"
import {
  ChakraProvider,
  Box,
  Text,
  Link,
  VStack,
  Code,
  Grid,
  theme,
} from "@chakra-ui/react"
import Sidebar from "./components/template/SideBar"
import { ColorModeSwitcher } from "./ColorModeSwitcher"

export const App = () => (
  <ChakraProvider >
    
    <Box>
    <Sidebar />
    </Box>
  </ChakraProvider>
)
